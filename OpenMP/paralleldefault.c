#include<stdio.h>
#include<omp.h>
omp_lock_t simple_lock;
int main()
{
	omp_init_lock(&simple_lock);
	int sharedval1=10;
	int sharedval2=10;
#pragma omp parallel default(shared)
	{
	 while (!omp_test_lock(&simple_lock));
	 sharedval1 = sharedval1+10;
	 sharedval2= sharedval2+30;
	 printf("Thread number %d Shared variable 1 value %d Shared variable 2 value %d\n", omp_get_thread_num() , sharedval1,sharedval2);
	 omp_unset_lock(&simple_lock);
	 
	}
	omp_destroy_lock(&simple_lock);
}
