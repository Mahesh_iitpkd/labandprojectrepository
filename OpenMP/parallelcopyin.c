#include<stdio.h>
#include<omp.h>
int prvsum;
# pragma omp threadprivate(prvsum)
int main()
{
#pragma omp parallel copyin(prvsum)
	{
		prvsum = prvsum+30;
		printf("Thread number %d value of sum %d \n",omp_get_thread_num(),prvsum);
	}
	}
