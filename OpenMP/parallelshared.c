#include<stdio.h>
#include<omp.h>
omp_lock_t simple_lock;
int main()
{
	omp_init_lock(&simple_lock);
	int sharedval=10;
#pragma omp parallel shared(sharedval)
	{
	 while (!omp_test_lock(&simple_lock));
	 sharedval = sharedval+10;
	 printf("Thread number %d Shared variable value %d\n", omp_get_thread_num() , sharedval);
	 omp_unset_lock(&simple_lock);
	 
	}
	omp_destroy_lock(&simple_lock);
}
