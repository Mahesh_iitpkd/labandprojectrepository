#include<stdio.h>
#include<omp.h>
int main()
{
	int product=1;
	
#pragma omp parallel reduction(*:product)
	{
	product=product*10;
	printf("Thread value is %d product value is %d\n",omp_get_thread_num(),product);
	}
	printf("Final Product = %d\n",product);
}
