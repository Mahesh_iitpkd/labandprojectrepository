#include<stdio.h>
#include<omp.h>
int main()
{
int sum=0;
#pragma omp parallel private(sum)
{
	sum = sum+10;
	printf("Thread is %d sum value is %d \n",omp_get_thread_num(),sum);
}
}
