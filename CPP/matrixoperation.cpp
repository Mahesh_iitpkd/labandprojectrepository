//Method defination of matrixoperation class.
#include<iostream>
#include<fstream>
#include<string.h>
#include <ctype.h>
#include <unistd.h>
#include "matrix.h"
#include "matrixoperation.h"
using namespace std;
  matrixOperation matrixOperation::operator + (int scaler)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] + scaler; 
   }
   }
   return resultmatrix;

  }
  matrixOperation matrixOperation::operator + (matrixOperation inputmatrix)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] + inputmatrix.matrix[i][j]; 
   }
   }
   return resultmatrix;

  }
  matrixOperation matrixOperation::operator - (int scaler)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] - scaler; 
   }
   }
   return resultmatrix;

  }
  matrixOperation matrixOperation::operator - (matrixOperation inputmatrix)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] - inputmatrix.matrix[i][j]; 
   }
   }
   return resultmatrix;

  }
  
  matrixOperation matrixOperation::operator * (int scaler)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] * scaler; 
   }
   }
   return resultmatrix;

  }
  matrixOperation matrixOperation::operator * (matrixOperation inputmatrix)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=inputmatrix.column;
   int i,j,n,sum;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->column;i++)
   {
   for(n=0;n<inputmatrix.column;n++)
   {
   	sum=0;
	for(j=0;j<this->column;j++)
	{
	sum= sum + (this->matrix[i][j] * inputmatrix.matrix[j][n]);
	}
	resultmatrix.matrix[i][n]=sum;
   }
   }
   return resultmatrix;

  }
  
void matrixOperation::det_cofact(int **cofact,int **mat,int f,int n){

int i = 0, j = 0;
   for (int row = 0; row < n; row++) {
      for (int col = 0; col < n; col++) {
         if (row != 0 && col != f) {
            cofact[i][j++] = mat[row][col];
            if (j == n-1) {
               j = 0;
               i++;
            }
         }
      }
   }
}
  int matrixOperation::det(int **mat, int n)
{

    int D,sign,i,j;
if (n == 1) {
      return mat[0][0];
   }
   if (n == 2) {
      return (mat[0][0] * mat[1][1]) - (mat[0][1] * mat[1][0]);
   }
 
    int **cofact;
    D=0;
    sign = 1;
    cofact=new int *[n-1];
    for(i=0;i<n-1;i++)
    {
    cofact[i]=new int[n-1];
    }
    
    
    for (int f = 0; f <n; f++)
    {
    	
        det_cofact(cofact,mat,f, n);
        D += sign * mat[0][f]* det(cofact, n - 1);
        sign = -sign;
    }
 
    return D;
}

 matrixOperation matrixOperation::operator / (int scaler)
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->row;
   resultmatrix.column=this->column;
   int i,j;
   resultmatrix.matrix=new int*[row];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[column];
			}
   for(i=0;i<this->row;i++)
   {
   for(j=0;j<this->column;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[i][j] / scaler; 
   }
   }
   return resultmatrix;

  }
  
  matrixOperation matrixOperation::transpose ()
  {
   matrixOperation resultmatrix;
   resultmatrix.row= this->column;
   resultmatrix.column=this->row;
   int i,j;
   resultmatrix.matrix=new int*[column];
		for(i=0;i<resultmatrix.row;i++)
			{
			  resultmatrix.matrix[i]= new int[row];
			}
   for(i=0;i<this->column;i++)
   {
   for(j=0;j<this->row;j++)
   {
   resultmatrix.matrix[i][j] = this->matrix[j][i]; 
   }
   }
   return resultmatrix;

  }
