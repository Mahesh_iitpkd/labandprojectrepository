//This class contains methods for handling scaler amtrix and matrix matrix operations.It inherits matrix class.
#ifndef MATRIXOPERATION_H
#define MATRIXOPERATION_H
class matrixOperation : public matrix{
public:
  matrixOperation operator + (int scaler);//'+' Operator overloaded to perform scaler matrix addition
  matrixOperation operator + (matrixOperation inputmatrix);//'+' Operator overloaded to perform matrix matrix addition
  matrixOperation operator - (int scaler);//'-' Operator overloaded to perform scaler matrix subtraction
  matrixOperation operator - (matrixOperation inputmatrix);//'-' Operator overloaded to perform matrix matrix subtraction
  
  matrixOperation operator * (int scaler);//'*' Operator overloaded to perform scaler matrix multiplication
  matrixOperation operator * (matrixOperation inputmatrix);//'*' Operator overloaded to perform matrix matrix multiplication
void det_cofact(int **cofact,int **mat,int f,int n);//function called in det function to compute determinant of function
  int det(int **mat, int n);//Function used to compute detrminant of matrix

 matrixOperation operator / (int scaler);//'/' operator overloaded to perform scaler matrix division
  matrixOperation transpose ();//Function to compute transpose of a matrix
  };
#endif
