//Class which is used to store matrix dimension and matrix values.and methods to create and print matrix.
#ifndef MATRIX_H
#define MATRIX_H
class matrix{
public:
	int row,column;
	int **matrix;
	void createMatrix(std::string read, int pos);//Function to create matrix
        void print(); //Function to print matrix
        int comment_space(std::string row);     //Function to check if line contains space or comment                 				
			
};
#endif
