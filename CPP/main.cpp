#include<iostream>
#include<fstream>
#include<string.h>
#include <ctype.h>
#include <unistd.h>
#include "matrix.h"
#include "matrixoperation.h"
#include "log.h"
using namespace std;
//Function to check if line read from inputstream is scalar or matrix specification
int checkscalar(string row)
		{
		int pos;
			for(int i=0;i<row.length();i++)
			{
				if(row[i] == ' ')
					{
						pos=i;
					return pos;
					}

		
			}
			return 0;
		}
//Function to check if line read from inputstream is comment or space
int checkcomment_space(string row)
		{
			int value=1;
                                if(row[0] == '#')
                                        return 1;
                  		for(int i=0;i<row.length();i++)
                  		{
                  		  if(row[i] !=' ')
                  		  {
                  		   value=0;
                  		   break;
                  		  }
                  		}              
                        return value;

		}

//Main function taking command line argument as string
int main(int argc, char **argv)
{
string read;
int scaler,i,pos,matrixcount,opt,initialize,nullinput,determinantflag;
matrixOperation mat[2];
scaler =0;
initialize=0;
nullinput=0;
determinantflag=0;
//Initializing logger by giving log file name
initLogger( "mylogfile.log", ldebug);
getline(cin,read);
try
{
//Read inputstream until a valid input is passed
while ((checkcomment_space(read)))
{
getline(cin,read);
//exit loop if end of file is encountered without any valid input
if(cin.eof())
  {
  nullinput=1;
  break;
  }
}
//Check if valid input has space and return position of space
pos= checkscalar(read);
//check if valid input is matrix
if((!(checkcomment_space(read))) && (checkscalar(read)))
{
mat[0].createMatrix(read,pos);
L_(linfo) << "Initialized first operand as matrix";
}
else
{
//Assign scaler ot input
scaler=stoi(read);
L_(linfo) << "Initialized first operand as scaler";
}
}
//Catch block to handle error while initializing first operand
catch(...)
{
 L_(lerror) << "Process exit due to Invalid input";
 //Print error message in log file and exit process as first input is invalid so operations cannot be performed
 return 1;
}
 
 //Read command line argument one character at a time
 while ((opt = getopt (argc, argv, "asmdtx")) != -1)
 {
 try
 {
 //If operator is transpose or determinant then no need to read new input
 if(opt != 't' && opt !='x')
 {
  getline(cin,read);
  while(checkcomment_space(read)){
  getline(cin,read);
  if(cin.eof())
  {
  nullinput=1;
  break;
  }
  }
  if(nullinput!=1)
  {
  //Check if next operand is matrix and initialize next operand 
  if(checkscalar(read))
  {
  	
   mat[1].createMatrix(read,pos);
   L_(linfo) << "Initialized next operand as matrix";
   //If last operation was not deteminant then next operand should be matrix matrix operation so make scaler variable as zero
   if(determinantflag !=1)
   {
   scaler=0;
   }
   else
   {
   //As last operation was determinant so next operation is scaler operation and store new input as matrix operand for scaler operation.
   mat[0]=mat[1];
   determinantflag=0;
   }
  }
  else if(scaler ==0)
  {
  scaler=stoi(read);
  L_(linfo) << "Initialized next operand as scaler";
  }
  else
  {
  //Error message is logged if we already have an scaler operand and next operand is scaler
  L_(lerror) << "Scaler Scaler operation is invalid";
  nullinput =1;
  }
 }
 }
 //If previous operation failed then rest all operations should fail.
 if(nullinput ==1)
 {
 opt='e';
 }
 //Switch case to execute a operation as per operator
  switch(opt)
  {
   case 'a':
   //Check if scaler variable has value
   if (scaler !=0)
   	{
               mat[0] = mat[0] + scaler;
               
               L_(linfo) << "Scaler Addition operation complete";
               cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
               mat[0].print();    
   	}
   else
   {
   if((mat[0].row==mat[1].row)&&(mat[0].column==mat[1].column))
   {
    mat[0] = mat[0] + mat[1];
    
    L_(linfo) << "Matrix Addition operation complete";
    cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
    mat[0].print();
    }
    else
    {
      L_(lerror) << "Matrix addition operation failed";
      L_(lerror) << "Invalid inputs for Matrix addition";
      nullinput =1;
    } 
   }
   break;
   case 's':
   if (scaler!=0)
   	{
               mat[0] = mat[0] - scaler;
               L_(linfo) << "Scaler Subtraction operation complete";
               cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
               mat[0].print();    
   	}
   else
   {
   if((mat[0].row==mat[1].row)&&(mat[0].column==mat[1].column))
   {
    mat[0] = mat[0] - mat[1];
    L_(linfo) << "Matrix Subtraction operation complete";
    cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
    mat[0].print(); 
   }
   else
   {
   L_(lerror) << "Matrix subtraction operation failed";
   L_(lerror) << "Invalid inputs for Matrix subtraction";
   nullinput =1;
   }
   }
   break;
   
   case 'm':
   if (scaler !=0)
   	{
               mat[0] = mat[0] * scaler;
               L_(linfo) << "Scaler Multiplication operation complete";
               cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
               mat[0].print();            
   	}
   else
   {
   if(mat[0].column == mat[1].row )
   {
    mat[0] = mat[0] * mat[1];
    L_(linfo) << "Matrix Multiplication operation complete";
    cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
    mat[0].print(); 
   }
   else
    {
         L_(lerror) <<"Matrix Multiplication operation failed";
    	 L_(lerror) << "Invalid inputs for Matrix multiplication";
    	 nullinput =1;
    }
    
   }
   break;
   case 'd':{
   if(scaler !=0)
   {
   	       mat[0] = mat[0] / scaler;
   	       L_(linfo) << "Scaler Division operation complete";
               cout<<"\n"<<mat[0].row<<" "<<mat[0].column<<"\n";
               mat[0].print();      
   }
   else
   {
   L_(lerror) << "Scaler division operation failed";
   L_(lerror) << "Invalid scaler value";
   nullinput =1;
   }
   break;
   }
   case 'x':
   {
               if(mat[0].row == mat[0].column)
               {
               int determinant = mat[0].det(mat[0].matrix,mat[0].row);
               L_(linfo) << "Matrix Determinant operation complete";
               cout<<"\n"<<determinant<<"\n"; 
               //Store determinant value as scaler for next operation
               scaler=determinant; 
               //Flag to remember during next operation that last operation was determinant
               determinantflag =1;
               } 
               else
               {
                L_(lerror) << "Matrix determinant operation failed";
                L_(lerror) << "Invalid matrix for determinant";
                nullinput =1;
               }          

   break;
   }
   case 't':
   {
               mat[0] = mat[0].transpose();
               L_(linfo) << "Matrix Transpose operation complete";
               cout<<"\n" <<mat[0].row<<" "<<mat[0].column<<"\n";
               mat[0].print();         

   break;
   }
   default:
   {
    L_(lerror) << "Operation failed due to Invalid Input";
    nullinput =1;
   }
  }
  }
  //Catch block to continue read of next operation but print error message
  catch(const exception & e)
  {
   L_(lerror) << "Operation Failed" <<e.what();
   nullinput =1;	
  }
  
 }
return 1;
}
