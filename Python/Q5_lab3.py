import matplotlib.pyplot as sinplt
import numpy as nup
import math
import matplotlib.animation as anmi


class Sines:
    def __init__(self):
        self.radAngles = []  #List to store angles in radians
        self.sr = 0  #Object variable to store shift right value
        self.sl = 0   #Object variable to store shift left value
        self.thigh=360 #Initial high value of range
        self.tlow=0 #Initial low value of range
        self.lowoffset =0 #offset for lower value of range
        self.highoffset= 0 #offset for higher value of range
        self.fig = sinplt.figure() #create a figure which will be animated
        self.ax1 = self.fig.add_subplot(1, 1, 1) #create a subplot of figur
        self.call="default"
        self.status=""
        self.currlowOffset=0 #Variable to store lower value of offset when " " is pressed
        self.currhighoffset=0 #Variable to store higher value of offset when " " is pressed


    def addSine(self, degAngle):
        self.radAngles.append(degAngle * (nup.pi / 180)) #add angle in radians
    def show(self):
       col = ["blue", "orange", "darkviolet","deeppink","chocolate"] #List of line colours of different lines.We are considering 5 different colours
       colind=0  #variable to store  colour index
       for rad in self.radAngles: #Loop through all radian angles
            t = range(0, 360)  #Range for which we need to plot sine function in degrees
            z = [(i) * ((nup.pi) / 180) for i in t]#Range for which we need to plot sine function in radians
            x = [(i + rad - self.sr +self.sl) for i in z] #Calculate the angle for which we need to plot sine value against radian value.We compute the value for which we need to find sin by including shift left,shift right,current radian angle.
            sinplt.xlim((0), ((2 * nup.pi)))  # Set limit for x axis
            sinplt.plot(z, nup.sin(x), '{}'.format(col[colind%5]), label="\u03A6 = {}\u00b0".format(math.ceil((rad*(180/nup.pi))))) # Plot sin function
            colind+=1
            sinplt.legend(ncol=2) # Display angles in a single row
            sinplt.ylim(-2, 2) # Set limit for y axis
            sinplt.title("Interactive sinusoidal functions")
            sinplt.axhline(y=1, color="green") # Horizontal line to mark maximum value
            sinplt.axhline(y=-1, color="red")  # Horizontal line to mark minimum value
            sinplt.axhline(y=0, color="black")  # Horizontal line to mark zero value
            sinplt.text(0.785, -1.15, "Minimum value") #Label Minimum value line
            sinplt.text(3.14, 1.15, "Maximum value") #Label Maximum value line
            sinplt.xlabel("\u03B8") #Label x axis
            sinplt.ylabel("sin(\u03B8 + \u03A6)") #Label y axis
            sinplt.xticks(nup.arange(min(z), max(z) + (0.25 * (nup.pi)), (0.25 * (nup.pi)))) # Mark ticks at distance of 0.25pi
            loc, text = sinplt.xticks()
            label = ["{0:.2f}\u03C0".format(lb / nup.pi) for lb in loc] #Label X ticks to display value with respect to pi
            sinplt.xticks(loc, label)
       sinplt.grid(linestyle='--')
       sinplt.show()
    def shiftRight(self,degAngle):
        self.sr = self.sr + degAngle*(nup.pi/180) # Calculate  angle to shift  right in radians
    def shiftLeft(self,degAngle):
        self.sl = self.sl + degAngle * (nup.pi / 180) # Calculate  angle to shift left in radians
#anime function which is called in matplot lib object to update the range of low and high values and plot the sin graph again
    def anime(self,i):
            col = ["blue", "orange", "darkviolet", "deeppink", "chocolate"]
            colind = 0
            self.ax1.clear()  # Clear the current subplot

            for rad in self.radAngles:
                z = [(((i) * ((nup.pi) / 180))) for i in range((self.tlow), (self.thigh))] #Range for which we need to plot sine function in radians
                x = [(i + rad-self.sr+self.sl ) for i in z] #Calculate the angle for which we need to plot sine value against radian value.We compute the value for which we need to find sin by including shift left,shift right,current radian angle.
                self.ax1.set_xlim(min(z), (max(z))) # Set limit for x axis

                self.ax1.plot(z, nup.sin(x), '{}'.format(col[colind % 5]),
                            label="\u03A6 = {}\u00b0".format(math.ceil((rad * (180 / nup.pi)))))
                colind += 1
                self.ax1.legend(ncol=2)
                self.ax1.set_ylim(-2, 2)
                self.ax1.grid(linestyle='--')
                self.ax1.axhline(y=1, color="green") # Horizontal line to mark maximum value
                self.ax1.axhline(y=-1, color="red")  # Horizontal line to mark minimum value
                self.ax1.axhline(y=0, color="black")  # Horizontal line to mark zero value
                self.ax1.text(min(z), -1.15, "Minimum value")# Dynamically place text of minimum vaue
                self.ax1.text((min(z) +(0.8*nup.pi)), 1.15, "Maximum value") # Dynamically place text of minimum vaue
                self.ax1.set_xlabel("\u03B8")
                self.ax1.set_ylabel("sin(\u03B8 + \u03A6)")
                sinplt.xticks(nup.arange(min(z), max(z)+(0.25 * (nup.pi)), (0.25 * (nup.pi))))
                loc, text = sinplt.xticks()
                label = ["{0:.2f}\u03C0".format(lb / nup.pi) for lb in loc]
                sinplt.xticks(loc, label)
                self.ax1.set_title("Interactive sinusoidal functions")
            self.tlow = self.tlow + self.lowoffset
            self.thigh = self.thigh + self.highoffset

    #interact function which is used to make the sin wave interactive. When we try to shift right or left we set offset values accordingly in lowoffset and highoffset variables of object.This lowoffset is used to alter lower value of range of values to plot and high offset is used to alter higher value of range of values to plot
    def interact(self):
        col = ["blue", "orange", "darkviolet", "deeppink", "chocolate"]
        colind = 0
        # changeflow inner function is used to alter the offset based on key press during the sinwave traversal.
        def changeflow(event):
            if event.key == 'd':  #If key pressed is 'd' then lowoffset and highoffset are updatd so that waves move to right
                if ((self.lowoffset != -18) and (self.highoffset != -18)):
                    self.lowoffset =  -18
                    self.highoffset = - 18
                    self.status = ""
            elif event.key == 'a':  #If key pressed is 'a' then lowoffset and highoffset are updated so that waves move to left
                if ((self.lowoffset != 18) and (self.highoffset != 18)):
                    self.lowoffset = 18
                    self.highoffset = 18
                    self.status = ""
            elif event.key == ' ':#If key pressed is ' ' then lowoffset and highoffset are updated  to zero (if spcebar is pressed odd number of times) or to original offset value(if spacebar is pressed even number of times) so that waves pause or resume movement
                if(not(self.status == "pause")):
                    self.currlowOffset = self.lowoffset
                    self.currhighoffset = self.highoffset
                    self.lowoffset = 0
                    self.highoffset = 0
                    self.status = "pause"
                else:
                    self.lowoffset = self.currlowOffset
                    self.highoffset = self.currhighoffset
                    self.status = ""
            elif event.key =='r': # If key pressed is 'r then graph is reset to original graph
                self.thigh = 360
                self.tlow = 0
                self.lowoffset = 0
                self.highoffset = 0
                self.status = ""
            else:
                pass



        an = anmi.FuncAnimation(self.fig, self.anime, interval=100)   #Creating Animation object and passing self.anme as calling finction
        self.fig.canvas.mpl_connect('key_press_event', changeflow)
        sinplt.show()

s = Sines()
s.addSine(0)
s.addSine(30)
s.addSine(45)
s.shiftRight(45)
s.interact()