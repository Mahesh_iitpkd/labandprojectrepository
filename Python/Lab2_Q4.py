# Polygon class which has two attributes (numSides and area ) ,constructor and overloaded __str__(self) function
class Polygon :
    def __init__(self,numsides,area):                                           # Constructor method
        try:
            if(numsides < 3) :                                                  # Condition to check if number of sides is less than 3
                raise Exception('Number of sides should be atleast 3')
            elif(area <=0) :                                                    # Condition to check if area is non-zero
                raise Exception('Polygon should have positive area')
            else:
                self.numSides = numsides
                self.area = area
        except Exception as p:                                                  # except block to handle any exception raised while initialising the attributes
            print(type(p))                                                      # Print statement to print type of exception
            print(p)                                                            # Print statement to print the exception message
    def __str__(self):                                                          # Overloading of predifined method of print
        try:                                                                    # Try block to handle exception raised if an object which is not initialized properly is passed
            return "Polygon with {} sides and area {}".format(self.numSides,self.area)
        except Exception as invpol:
            return "Invalid Polygon"
# Triangle class which inherits polygon class
class Triangle(Polygon) :
    def __init__(self,side1,side2,side3):                                       # Constructor to initialize attributes (numSides,area) of base class (Polygon)
        try:
            if ((side1 < 0) | (side2 < 0) | (side3 < 0)):                       # Condition to check if side has length less than zero
                raise Exception("Triangle should have positive side-lengths")

            elif  ((side1+side2 <= side3) |(side2+side3 <= side1) | (side1+side3 <= side2) ):   # Condition to check if sum of any two sides is gless than third side
                raise Exception("Side-lengths do not form a triangle")
            else:
                self.numSides = 3                                               # Assign number of sides to attribute of base class (Polygon)
                self.area = ((((side1 + side2 + side3) / 2)*(((side1 + side2 + side3) / 2)-(side1))*(((side1 + side2 + side3) / 2)-(side2))*(((side1 + side2 + side3) / 2)-(side3))) ** 0.5)            # Calculate area of a triangle and assign it to area attribute of base class (Polygon)
        except Exception as t:                                                  # except block to handle any exception raised while initialising the attributes
            print(type(t))                                                      # Print statement to print type of exception
            print(t)                                                            # Print statement to print the exception message
    def __str__(self):                                                          # Overloading of predifined method of print
        try:                                                                    # Try block to handle exception raised if an object which is not initialized properly is passed
             return "Triangle with area {}".format(self.area)
        except Exception as invtri:
            return "Invalid Triangle"
# Paper class
class Paper:
    def __init__(self,area):                                                    # Constructor to check if area of paper is negative
        try:
            if(area <0):                                                        # Condition to check if area of paper is positive
                raise Exception("Paper should have a positive area")
            else:
                self.originalArea= area                                         # Storing original area which can be used during printing description of paper
                self.area=area                                                  # Storing area of paper which can be used in calculation when a new polygon is added to paper
                self.polygons=[]                                                # List to store all Polygon objects associated with a Paper object
                self.triangles=[]                                               # List to store all Triangle objects associated with  a Paper object
        except Exception as p:
            print(type(p))
            print(p)
    def __add__(self, other):                                                   # Method to overload + operator which can take one object of Paper class and other object of Polygon or Triangle class
        try:
            if(self.area-other.area <0):                                        # Condition to check if area of the Polygon/Triangle is grter thn available area in Paper
               raise Exception("Paper does not have sufficient free area to fit the polygon")
            else:
                if (other.numSides !=3):                                        # Condition to check if Polygon does not have 3 sides
                    self.polygons.append(other)                                 # Adding Polygon object to Polygon list
                    self.area -= other.area                                     # Reducing area of Paper after addition of Polygon
                    return self
                else:
                    self.triangles.append(other)                                # Adding Triangle object to Triangle list
                    self.area -= other.area                                     # Reducing rea of Paper after addition of Triangle
                    return self                                                 # Returning Paper object
        except Exception as invadd:                                             # except block to handle any exception raised during addition of Polygon/Triangle to paper
            print(type(invadd))
            print(invadd)
    def __str__(self):                                                          # Overloading of predifined method of print
        try:
            papDesc="Paper has free area {} out of {}, and contains:".format(self.area,self.originalArea)       # String variable to store paper description.Free area and total area are added to the string

            for poly in self.polygons:
                papDesc= papDesc +"\nPolygon with {} sides and area {}".format(poly.numSides,poly.area)         # Adding details of all Polygons which includes polygon side and polygon area to paper description.
            for tri in self.triangles:
                papDesc = papDesc + "\nTriangle with area {}".format(tri.area)                                  # Adding detais of all Triangles which includes area of triangles to paper description
            return papDesc
        except Exception as invPap :                                                                            # except block which handles exceptions raised during addition of pper description
            return(invPap)
    def merge(self):                                                                                            # Merge method to merge the polygon of equal sides into one polygon and all traingles into single triangle
        for poly in self.polygons:                                                                              # Outer Polygon loop :-Loop through all existing Polygon object in List
            for polysame in self.polygons:                                                                      # Inner Polygon loop :-Loop through all existing Polygon object in List
                if((poly.numSides == polysame.numSides) & (id(poly) != id(polysame))):                          # Condition to check if number of side of Polygon object in Inner Polygon loop is equal to number of side of Polygon object of Outer Polygon loop.Also to ommit the polygon object in Outer Polygon loop
                    poly.area += polysame.area                                                                  # Combining area of Polygon object of Inner Polygon loop with  area of Polygon object of Outer Polygon loop with same side
                    self.polygons.remove(polysame)                                                              # Removing the polygon object in Inner Polygon loop which has side equal to polygon object of Outer Polygon loop
        for tri in self.triangles:                                                                              # Outer Triangle loop :- Loop through all existing Triangle object in List
            for trisame in self.triangles:                                                                      # Inner Triangle loop :- Loop through all existing Triangle object in List
                if(id(tri) != id(trisame)):                                                                     # Condition to skip object of Outer Triangle loop
                    tri.area +=  trisame.area                                                                   # Adding area of Triangle of Inner Triangle loop object to area of Triangle of Outer Triangle loop object
                    self.triangles.remove(trisame)                                                              # Removing object of Inner Triangle loop
    def erase(self):                                                                                            # erase method which is used to remove all shapes in current paper and restore paper area.
        for poly in self.polygons :                                                                             # Loop through all the Polygon objects in Polygon List
            self.area += poly.area                                                                              # Add area of polygon to original area
        self.polygons.clear()                                                                                   # Remove all polygons from Polygon list
        for tri in self.triangles :                                                                             # Loop throgh all Triangle objects in Triangle list
            self.area += tri.area                                                                               # Add area of triangle to original area
        self.triangles.clear()                                                                                  # Remove all triangles from Triangle list

pap = Paper(200)
pap = Paper(-1)

pap = Paper(200)
p = Polygon(10, 100)
pap = pap + p
pap = pap + Polygon(5, 45)
print(type(pap).__name__)

pap = Paper(200)
p1 = Polygon(10, 100)
p2 = Polygon(20, 150)
pap = (pap + p1) + p2

pap = Paper(200)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Triangle(3, 4, 5)
print(pap)

pap = Paper(200)
pap = pap + Polygon(3, 100)
print(pap)

pap = Paper(2000)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Polygon(10, 200)
pap = pap + Polygon(3,24)
pap = pap + Triangle(3, 4, 5)
print(pap)
pap.merge()
print(pap)
pap = pap + Polygon(10, 123)
print(pap)

pap = Paper(200)
pap = pap + Polygon(10, 100)
pap = pap + Polygon(20, 50)
pap = pap + Triangle(3, 4, 5)
print(pap)
pap.erase()
print(pap)
