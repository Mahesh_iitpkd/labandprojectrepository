import concurrent.futures
import threading
import time
import random
producerwait=threading.Event()   #Producer event to control production of events 
consumerwait=threading.Event()   #Consumer event to control consumption of events 
class eventmanager():
    def __init__(self):
        self.waittime={1,2,3,4,5}
        self.message="EventCreated"
    def EG(self):
        producerwait.set()
        for out in range(1,11):

            set= self.timer + random.choice(list(self.waittime))  #set the wait time with respect to current time
            print(" Time {}s : Event is scheduled at {}s".format(self.timer, set))
            while (not (set == self.timer)): #wait till calculated time
                pass
            print(" Time {}s : Event occurred".format(self.timer))
            self.message="EventCreated"
            consumerwait.set()  #Release consumer lock to consume event
            producerwait.clear()  #Reset producer lock 
            producerwait.wait()  #Lock producer till consumer consumes event
            print(" Time {}s : Event processed".format(self.timer))
        self.message="done"
    def EC(self):

        while (not(self.message=="done")):  #     check if producer has created all events
            consumerwait.wait() # Wait till producer creates event
            wait=random.choice(list(self.waittime))
            time.sleep(wait) #Sleep for calculated wait time
            producerwait.set()  #Release producer lock to produce new events
            consumerwait.clear() #Reset Consumer lock
            time.sleep(100/1000)

    def time(self):
        self.timer = -1;
        while (not(self.message == "done")):
            self.timer = self.timer + 1
            time.sleep(1)

em=eventmanager()
with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
    t1 = executor.submit(em.time)
    p2 = executor.submit(em.EG)
    p1= executor.submit(em.EC)


