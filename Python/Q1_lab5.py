import tkinter as tik
from tkinter import ttk
import time as t
import random

def progressChange(): #Function caled on button click to update progress bar value and lable text
    delay = {1, 2, 3, 4, 5} #List of time to randomly select delay
    progress['value']=0
    progresslabel.configure(text="Current Progress : 0.0%")
    button.configure(state="disabled") # Disable button once clicked
    entry.configure(state="disabled") # Disable entry once clicked
    window.update_idletasks()
    size=entry.get()
    increment=100/int(size) #Calculate increment value on download of file
    processed=increment
    while(int(size)>0):
        window.update_idletasks()
        sl = random.choice(list(delay))
        t.sleep(sl)
        progress['value'] = progress['value'] + increment #Increment value in progresbar
        progresslabel.configure(text="Current Progress : {}%".format(int(processed)))#Update label
        processed=processed+increment
        size=int(int(size)-1)

    progresslabel.configure(text="Download complete")#Final text in label after download of all files
    button.configure(state="active")#Enable button
    entry.configure(state="active")#Enable entry

window = tik.Tk()
window.title("Progressbar Demo")
frame=tik.Frame(master=window,borderwidth=1)
progress = ttk.Progressbar(window,orient='horizontal', length=200, mode='determinate')

progress.pack()
frame2=tik.Frame(master=window,borderwidth=1)
progresslabel = tik.Label(window,text="Current Progress : 0.0%")
progresslabel.pack()
greetinglabel = tik.Label(window,text="No. of files")

greetinglabel.pack(side=tik.LEFT,padx=10)
entry = ttk.Entry(width=5)
entry.pack(side=tik.LEFT,padx=10) #Place entry next to label

button = tik.Button(window,text="Start",command=progressChange)

button.pack(side=tik.LEFT,padx=40) #Place button next to entry

window.mainloop()

