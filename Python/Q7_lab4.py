import socket
import json
import concurrent.futures
import threading
import time
import random

l = threading.Lock()
class Network():
    def __init__(self):
        self.host= '127.0.0.1'
        self.port =  5891
        self.connect=[]
        self.i=-1
        self.message=''
        self.threads=1
        self.serverDelay={1,2,3,4,5}
        self.timer= time.monotonic() #Monotonic function to store time at begining of execution

    def serveraction(self): #Method to be executed during each call from client to server
        value = self.i
        sl=random.choice(list(self.serverDelay)) #Random value to sleep before execution

        time.sleep(sl) #Sleep for random selected time
        with self.connect[value]:
            tuple_json = self.connect[value].recv(1024) #Receive data from client
            self.connect[value].sendall(bytes(tuple_json)) #Send resposne to client
    def server(self): # Server Method

            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

                s.bind((self.host, self.port)) #Bind host and port to server socket

                s.listen() #Make server to listen to client connections

                while (True):
                    conn, addr = s.accept() #Accept client connections
                    s.setblocking(False) #Make each client connection as non blocking to accept multple client connections
                    self.i = self.i + 1
                    self.connect.append(conn) #Add connection to list

                    serv=threading.Thread(target=self.serveraction) #Create a new thread for client connection

                    serv.start() #Start new thread to process client data

    def client(self,name): #Client method
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                while(True): #Loop to handle failure connection to server
                    try:
                        s.connect((self.host, self.port)) #Connect to server
                        msg="Hello from thread {}".format(name)
                        send_server=json.dumps(msg) #Convert request to Json object
                        s.sendall(bytes(send_server, encoding="utf-8")) #Send JSON object to server
                        with l:
                            print("Time {}s: Thread {} sent message to server".format(int(time.monotonic()-self.timer),name))
                            print("Msg sent by thread {}: {}".format(name,msg))
                        server_tuple = s.recv(1024) #Receive message from server
                        server_tuple_dec=server_tuple.decode("utf-8")
                        server_data=json.loads(server_tuple) #Convert Json data from server
                        with l:
                            print("Time {}s: Thread {} received message from server".format(int(time.monotonic()-self.timer),name))
                            print("Msg received by thread {}: {} ".format(name,server_data))
                        break
                    except exception as e:
                        print(e)


n=Network()
with concurrent.futures.ThreadPoolExecutor(max_workers=11) as executor: #Create threads for single server call and 10 client method call
     t1 = executor.submit(n.server)
     p1 = executor.submit(n.client,1)
     p2 = executor.submit(n.client, 2)
     p3 = executor.submit(n.client, 3)
     p4 = executor.submit(n.client, 4)
     p5 = executor.submit(n.client, 5)
     p6 = executor.submit(n.client, 6)
     p7 = executor.submit(n.client, 7)
     p8 = executor.submit(n.client, 8)
     p9 = executor.submit(n.client, 9)
     p10 = executor.submit(n.client, 10)
