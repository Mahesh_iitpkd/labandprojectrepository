import matplotlib.pyplot as sinplt
import numpy as nup
import math
class Sines :
    def __init__(self):
        self.radAngles=[]   #Object variable to store angle in radians
        self.sr=0           #Object variable to store shift right value
        self.sl=0           #Object variable to store shift left value
    def addSine(self,degAngle):
        self.radAngles.append(degAngle*(nup.pi/180)) #add angle in radians
    def show(self):
       col = ["blue", "orange", "darkviolet","deeppink","chocolate"] #List of line colours of different lines.We are considering 5 different colours
       colind=0  #variable to store  colour index
       for rad in self.radAngles: #Loop through all radian angles
            t = range(0, 360)  #Range for which we need to plot sine function in degrees
            z = [(i) * ((nup.pi) / 180) for i in t]#Range for which we need to plot sine function in radians
            x = [(i + rad - self.sr +self.sl) for i in z] #Calculate the angle for which we need to plot sine value against radian value.We compute the value for which we need to find sin by including shift left,shift right,current radian angle.
            sinplt.xlim((0), ((2 * nup.pi)))  # Set limit for x axis
            sinplt.plot(z, nup.sin(x), '{}'.format(col[colind%5]), label="\u03A6 = {}\u00b0".format(math.ceil((rad*(180/nup.pi))))) # Plot sin function
            colind+=1
            sinplt.legend(ncol=2) # Display angles in a single row
            sinplt.ylim(-2, 2) # Set limit for y axis
            sinplt.title("Interactive sinusoidal functions")
            sinplt.axhline(y=1, color="green") # Horizontal line to mark maximum value
            sinplt.axhline(y=-1, color="red")  # Horizontal line to mark minimum value
            sinplt.axhline(y=0, color="black")  # Horizontal line to mark zero value
            sinplt.text(0.785, -1.15, "Minimum value") #Label Minimum value line
            sinplt.text(3.14, 1.15, "Maximum value") #Label Maximum value line
            sinplt.xlabel("\u03B8") #Label x axis
            sinplt.ylabel("sin(\u03B8 + \u03A6)") #Label y axis
            sinplt.xticks(nup.arange(min(z), max(z) + (0.25 * (nup.pi)), (0.25 * (nup.pi)))) # Mark ticks at distance of 0.25pi
            loc, text = sinplt.xticks()
            label = ["{0:.2f}\u03C0".format(lb / nup.pi) for lb in loc] #Label X ticks to display value with respect to pi
            sinplt.xticks(loc, label)
       sinplt.grid(linestyle='--')
       sinplt.show()
    def shiftRight(self,degAngle):
        self.sr = self.sr + degAngle*(nup.pi/180) # Calculate  angle to shift  right in radians
    def shiftLeft(self,degAngle):
        self.sl = self.sl + degAngle * (nup.pi / 180) # Calculate  angle to shift left in radians

s = Sines()
s.addSine(0)
s.addSine(90)
s.show()
s = Sines()
s.addSine(0)
s.addSine(90)
s.shiftRight(45)
s.show()
s = Sines()
s.addSine(0)
s.addSine(90)
s.shiftRight(45)
s.shiftLeft(45)
s.show()