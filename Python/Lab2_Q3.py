# Polygon class which has two attributes (numSides and area ) ,constructor and overloaded __str__(self) function
class Polygon :
    def __init__(self,numsides,area):                                           # Constructor method
        try:
            if(numsides < 3) :                                                  # Condition to check if number of sides is less than 3
                raise Exception('Number of sides should be atleast 3')
            elif(area <=0) :                                                    # Condition to check if area is non-zero
                raise Exception('Polygon should have positive area')
            else:
                self.numSides = numsides
                self.area = area
        except Exception as p:                                                  # except block to handle any exception raised while initialising the attributes
            print(type(p))                                                      # Print statement to print type of exception
            print(p)                                                            # Print statement to print the exception message
    def __str__(self):                                                          # Overloading of predifined method of print
        try:                                                                    # Try block to handle exception raised if an object which is not initialized properly is passed
            return "Polygon with {} sides and area {}".format(self.numSides,self.area)
        except Exception as invpol:
            return "Invalid Polygon"
# Triangle class which inherits polygon class
class Triangle(Polygon) :
    def __init__(self,side1,side2,side3):                                       # Constructor to initialize attributes (numSides,area) of base class (Polygon)
        try:
            if ((side1 < 0) | (side2 < 0) | (side3 < 0)):                       # Condition to check if side has length less than zero
                raise Exception("Triangle should have positive side-lengths")

            elif  ((side1+side2 <= side3) |(side2+side3 <= side1) | (side1+side3 <= side2) ):   # Condition to check if sum of any two sides is gless than third side
                raise Exception("Side-lengths do not form a triangle")
            else:
                self.numSides = 3                                               # Assign number of sides to attribute of base class (Polygon)
                self.area = ((((side1 + side2 + side3) / 2)*(((side1 + side2 + side3) / 2)-(side1))*(((side1 + side2 + side3) / 2)-(side2))*(((side1 + side2 + side3) / 2)-(side3))) ** 0.5)            # Calculate area of a triangle and assign it to area attribute of base class (Polygon)
        except Exception as t:                                                  # except block to handle any exception raised while initialising the attributes
            print(type(t))                                                      # Print statement to print type of exception
            print(t)                                                            # Print statement to print the exception message
    def __str__(self):                                                          # Overloading of predifined method of print
        try:                                                                    # Try block to handle exception raised if an object which is not initialized properly is passed
             return "Triangle with area {}".format(self.area)
        except Exception as invtri:
            return "Invalid Triangle"

t=Triangle(3,2,1)

t=Triangle(1,-2,3)

t = Triangle(3, 4, 5)
print(t)



