# Polygon class which has two attributes (numSides and area ) ,constructor and overloaded __str__(self) function
class Polygon :
    def __init__(self,numsides,area):                                           # Constructor method
        try:
            if(numsides < 3) :                                                  # Condition to check if number of sides is less than 3
                raise Exception('Number of sides should be atleast 3')
            elif(area <=0) :                                                    # Condition to check if area is non-zero
                raise Exception('Polygon should have positive area')
            else:
                self.numSides = numsides
                self.area = area
        except Exception as p:                                                  # except block to handle any exception raised while initialising the attributes
            print(type(p))                                                      # Print statement to print type of exception
            print(p)                                                            # Print statement to print the exception message
    def __str__(self):                                                          # Overloading of predifined method of print
        try:                                                                    # Try block to handle exception raised if an object which is not initialized properly is passed
            return "Polygon with {} sides and area {}".format(self.numSides,self.area)
        except Exception as invpol:
            return "Invalid Polygon"


p=Polygon(10,23)
p=Polygon(1,23)
p=Polygon(10,-23)
p=Polygon(13,123)
print(p)