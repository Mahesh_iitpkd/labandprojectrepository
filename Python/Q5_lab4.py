import concurrent.futures
import threading
import time
import random

s=threading.BoundedSemaphore(2) # Semaphore object to allow only two threads at a time
l = threading.Lock()
class thread_Semaphore:
  def __init__(self):
     self.intime_set={1,2,3,4,5}
     self.outtime_set={5,6,7,8,9,10}
     self.out=0


  def time(self):
    self.timer=0;
    while (self.out !=5) :
        self.timer=self.timer+1
        time.sleep(1)
  def person(self,name):


          meettime=random.choice(list(self.intime_set)) #Random selection of arrival time at mall
          while (not(meettime==self.timer)):# Wait till selected arrival time
            pass
          with l:
            print(" Time {}s : Person {} reached the shop".format(self.timer,name))

          while (not(s.acquire())): # Wait till semaphore object is not acquired
              pass
          with l: #Lock to allow only one thread to print message
            print(" Time {}s : Person {} entered the shop".format(self.timer,name))
          stay=int(random.choice(list(self.outtime_set)))  # Random Select time to stay in mall
          leavetime = self.timer + stay

          while (not(leavetime==self.timer)): #Stay in mall till leave time
              pass
          with l:
            print(" Time {}s : Person {} left the shop ".format(self.timer, name))
            self.out=self.out+1
          s.release()

tb=thread_Semaphore()
with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
     t1 = executor.submit(tb.time)
     p1=executor.submit(tb.person, 1)
     p2 = executor.submit(tb.person, 2)
     p3 = executor.submit(tb.person, 3)
     p4 = executor.submit(tb.person, 4)
     p5 = executor.submit(tb.person, 5)
