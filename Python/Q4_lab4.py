import concurrent.futures
import threading
import time
import random

b=threading.Barrier(5) #Barrier object to check if all 5 threads have arrived
l = threading.Lock()
class thread_barrier:
  def __init__(self):
     self.intime_set={1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20}
     self.outtime_set={1,2,3,4,5,6,7,8,9,10}


  def time(self):
    self.timer=0;
    while (self.timer !=30) :
        self.timer=self.timer+1
        time.sleep(1)
  def person(self,name):
    try:

          meettime=random.choice(list(self.intime_set)) #Random selection of arrival time at mall
          leavetime= int(20)+int(random.choice(list(self.outtime_set))) #Random selection of exit time from mall
          while (not(meettime==self.timer)): #Wait till selected arrival time
            pass
          with l:
            print(" Time {}s : Person {} reached the mall".format(self.timer,name))
          b.wait() #Wait for other threads to arrive
          with l: # Lock to ensure only one thread prints
            print(" Time {}s : Person {} enters the mall".format(self.timer,name))

          while (leavetime !=0):
              if(leavetime==self.timer):
                  with l:
                      print(" Time {}s : Person {} leaves the mall".format(self.timer, name))
                  leavetime = 0

    except Exception as e:
        print(e)
tb=thread_barrier()
with concurrent.futures.ThreadPoolExecutor(max_workers=6) as executor:
     t1 = executor.submit(tb.time)
     p1=executor.submit(tb.person, 1)
     p2 = executor.submit(tb.person, 2)
     p3 = executor.submit(tb.person, 3)
     p4 = executor.submit(tb.person, 4)
     p5 = executor.submit(tb.person, 5)
