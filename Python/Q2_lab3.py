from matplotlib import pyplot as pypl
class PieChart:

    def __init__(self,piedat):  # Consturctor of Pie Chart class which checks the dictionary passed during initilization nd raises exception
        try:
           self.pieData={}
           for dat in piedat :
               if type(dat) is not str:  #If Key is not string then exception is raised
                raise Exception("Label should be string")
               elif ((type(piedat[dat])) is not int):  #If Value is not integer then exception is raised
                raise Exception("Value should be a postive numeric")
               elif (piedat[dat] <0) :  #If value is negative then exception is raised
                raise Exception("Value should be a postive numeric")
               else:
                   pass
           self.pieData = piedat.copy()

        except Exception as e:
            print(type(e))
            print(e)
            self.pieData.clear()
    def show (self):
        slices = [] #Create a blank list to store slices of pie chart
        for fr in self.pieData:
            slices.append(self.pieData.get(fr))  #Loop through dictionary of object and add data of a aprticular key into slice
        pypl.pie(slices,labels=self.pieData ,autopct='%1.1f%%') # Plot slices ,labels, % occupied by ech slice in pie chart
        pypl.show()
    def __add__(self, other):  #Override add method to add data to existing ictionary of object
        if type(other) is PieChart: # Check if Key passed is alrady present in dictionary of object
            for t in other.pieData:
                    self.pieData[t] = self.pieData.get(t,0) + other.pieData[t]  # If key is present just update value corresponding to key
        else:
            try:
                if type(other[0]) is not str: #Check if new key passed is not string
                    raise Exception("Label should be string")
                elif ((type(other[1])) is not int): # Check if value is not integer
                    raise Exception("Value should be a postive numeric")
                elif (other[1] < 0): # Check if value is negative
                    raise Exception("Value should be a postive numeric")
                else:
                    self.pieData[other[0]] = self.pieData.get(other[0],0) + other[1]  # Add data to the dictionary
            except Exception as d:
                print(type(d))
                print(d)

        return self
    def __sub__(self, other): # Override - operator to remove data of a particular key
       if(self.pieData.get(other)):# Check if key exists in the object dictionary
        del self.pieData[other] # Delete the key value pair
       return self



p = PieChart({1, 23})
p = PieChart({'Frog': '30'})
p = PieChart({'Frog': -10})



p = PieChart({'Frogs': 10, 'Dog': 20, 'Cat':30})
p.show()
p = PieChart({'Frogs': 10, 'Dog': 25})
p = p + ('Cat', 25)
p.show()
p = PieChart({'Frogs': 10, 'Dog': 25})
p = p + ('Dog', 25)
p.show()
p = PieChart({'Frogs': 10, 'Dog': 20})
p = p + PieChart({'Frogs': 20, 'Cat': 10})
p.show()
p = PieChart({'Frogs': 10, 'Dog': 20, 'Cat':30})
p = p - 'Frogs'
p = p - 'Lions'
p.show()
