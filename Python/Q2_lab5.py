import tkinter as ttk
window = ttk.Tk()
window.geometry("300x300")
canv=ttk.Canvas(window, width =300,height=300,bg="white" ) #Canvas window inside frame
canv.pack(pady=20)
hr_x=0
hr_y=140
hr=canv.create_rectangle(hr_x,hr_y,hr_x+75,hr_y+20,fill="black") #Horizontal paddle object
vr_x=75
vr_y=0
vr=canv.create_rectangle(vr_x,vr_y,vr_x+50,vr_y+50,fill="grey") #Vertical box ob ject
fallstop=0
def move(event):
    global hr,hr_x,hr_y,vr,vr_x,vr_y,fallstop,collision,rest
    if ((event.x in range((hr_x - 20), (hr_x + 76))) and (event.y in range((hr_y), (hr_y + 21)))): #Check if mouse pointer is near paddle

        if((event.x+75)>300) :#Restrict movement of paddle outside frame

            pass
        elif(event.x<1): #Restrict movement of paddle outside frame
           pass
        else:

                canv.delete(hr)#Delete paddle at current position
                hr_x = event.x

                hr = canv.create_rectangle(event.x, 140, event.x + 75, 160, fill="black") #Create paddle at pointer position
                if(rest):#Check if horizontal box is resting on paddle top surface
                    rest=False
                    movedown()#Call function to move horizontal box down

    #elif((event.x in range((vr_x - 20), (vr_x + 51))) and (event.y in range((vr_y), (vr_y + 51)))):
    else:

      if(collision == False):#Dont change posiiton to mouse pointer after colission
            if ((event.y + 50) > 300):

                pass
            elif (event.y < 1):
               pass
            elif(event.x<1):
                pass
            elif((event.x+50) >300):
                pass


            else:

                canv.delete(vr)
                vr_y = event.y
                vr_x= event.x

                if ((vr_x == (hr_x + 75)) and (((vr_y <= hr_y) and ((vr_y - hr_y) <= 70)) or (
                (((hr_y + 20) >= event.y) and (((hr_y + 20) - event.y) <= 70))))):#Check left colission with paddle

                    collision = True

                    vr = canv.create_rectangle((event.x),(event.y),event.x+50,event.y+50, fill="grey")
                    movedown()#Call function to move box down
                elif (((vr_x+50) == (hr_x)) and (((vr_y <= hr_y) and ((vr_y - hr_y) <= 70)) or (
                (((hr_y + 20) >= event.y) and (((hr_y + 20) - event.y) <= 70))))): #Check right colission with paddle

                    collision = True

                    vr = canv.create_rectangle((event.x),(event.y),event.x+50,event.y+50, fill="grey")
                    movedown()
                elif(((vr_y-2) in range(hr_y,hr_y+21))and(((vr_x<=hr_x)and((hr_x-vr_x)<=50))or(((hr_x+75)>=(vr_x+50))and(((hr_x+75)-(vr_x+50))<=25))or((((vr_x+50)>(hr_x+75)))and((vr_x+50)-(hr_x+75)<=50)))): #Check bottom colission with paddle


                    collision = True

                    vr = canv.create_rectangle((event.x),(event.y),event.x+50,event.y+50, fill="grey")
                    movedown()
                else:

                     vr = canv.create_rectangle((event.x),(event.y),event.x+50,event.y+50, fill="grey")
                     fallstop = 0


                     canv.bind('<B1-ButtonRelease>',movedownevent) #Call function to move box down after button release
      else:

           movedown()

def movedownevent(event):
    movedown()
def movedown():
    global vr_x,vr_y,fallstop,collision,vr,rest


    if(((vr_y+50) in range((hr_y-1),(hr_y+21)))and((vr_x in range((hr_x-1),(hr_x+76)))or((vr_x+50)in range((hr_x-1),(hr_x+76))))and (collision==False)): #Check if bottom of box collides with top of paddle

        rest=True #Set flag of rest

        pass

    else:
        #if(((vr_y<209) and ((vr_y+50)<261))):
        if((vr_y+50)<298):

            canv.move(vr, 0, 1)  #Move box down

            if((vr_y+50)>300):
                print("moving up")
                canv.move(vr,0,((vr_y+50)-300))
            window.update_idletasks()

            side = canv.bbox(vr)
            x1, y1, x2, y2 = side
            vr_x=x1
            vr_y=y1
            window.after(10,movedown)

        else:

            collision=False

collision=False
rest=False
canv.bind('<B1-Motion>',move) #Bind button click event to function
movedown()

window.mainloop()

