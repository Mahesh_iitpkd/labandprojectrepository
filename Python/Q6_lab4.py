import socket
import json
import concurrent.futures

import time
class Network():
    def __init__(self):
        self.host= '127.0.0.1'
        self.port =  5891
    def server(self): #Server method
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((self.host, self.port)) #Bind server socket with a host and port
            s.listen() # Listen to request on socket
            conn, addr = s.accept() #Accept request from socket and store value of connection and address of client
            with conn:
                tuple_json = conn.recv(1024)
                tuple_json_dec=tuple_json.decode("utf-8") #Decode json message from client
                a=()
                b=()
                tuple_data= json.loads(tuple_json_dec) #Translate Json object to Python object
                max_a=tuple_data[0][0]
                min_a=tuple_data[0][0]
                sum_a=0
                max_b = tuple_data[0][1]
                min_b = tuple_data[0][1]
                sum_b = 0
                for a in tuple_data:   #For loop which caluclates sum of first value in all tuples,max value among all first values in tuples, min value among all first vaues in tuples
                    sum_a=sum_a+a[0]
                    if(a[0]>max_a):
                        max_a=a[0]
                    if(a[0]<min_a):
                        min_a=a[0]
                a_avg=sum_a/10
                for b in tuple_data: #For loop which caluclates sum of second value in all tuples,max value among all second values in tuples, min value among all second vaues in tuples
                    sum_b=sum_b+b[1]
                    if(b[1]>max_b):
                        max_b=b[1]
                    if(b[1]<min_b):
                        min_b=b[1]
                b_avg=sum_b/10

                B=[(a_avg,b_avg),(max_a,max_b),(min_a,min_b)]
                conn.sendall(bytes(json.dumps(B),encoding="utf-8")) #Convert tuple to Json object and send JSON object to client
    def client(self): # CLient method

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((self.host, self.port)) # Connect to server host and port
            A=[(1,23),(29,34),(36,7),(104,2),(89,35),(92,5),(25,9),(103,245),(30,64),(5,91)]
            send_server=json.dumps(A) #Convert tuple to json object
            s.sendall(bytes(send_server,encoding="utf-8")) #Send json object to server
            print("ClientDataSent - {}".format(A))
            server_tuple = s.recv(1024) #Receive data from server
            server_tuple_dec=server_tuple.decode("utf-8") #Decode data to json
            server_data=json.loads(server_tuple) # Convert JSON to python object
            print("ServerDataReceived - {}".format(server_data))

n=Network()
with concurrent.futures.ThreadPoolExecutor(max_workers=2) as executor:  #Execute server and client thread in parallel
     t1 = executor.submit(n.server)
     p1=executor.submit(n.client)
